package com.kiddiepay.kiddiepay.api;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public abstract class JsonObjectRequest extends Request<String, JSONObject> {
    public JsonObjectRequest(Listener<JSONObject> listener) {
        super(listener);
    }

    @Override
    protected Response.Listener<String> getListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JsonObjectRequest.this.mListener.onResponse(new JSONObject(response));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
    }

    @Override
    protected Map<String, String> getHeaders() {
        HashMap<String, String> headers = (HashMap<String, String>) super.getHeaders();
        headers.put("accept", "application/json");
        return headers;
    }

    protected com.android.volley.Request getHttpRequest() {
        return new StringRequest(getRequestMethod(), Client.BASE_URL.concat(getUrl()), getListener(),
                                 getErrorListener()) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return JsonObjectRequest.this.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return JsonObjectRequest.this.getParams();
            }
        };
    }
}
