package com.kiddiepay.kiddiepay.api;

import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.kiddiepay.kiddiepay.MainApplication;

import java.util.HashMap;
import java.util.Map;

/**
 * Generic request interface of application programming interfaces.
 */

public abstract class Request<T, K> {
    protected Listener<K> mListener;

    public Request(Listener<K> listener) {
        this.mListener = listener;
    }

    public abstract String getUrl();

    protected abstract int getRequestMethod();

    protected abstract Response.Listener<T> getListener();

    protected Response.ErrorListener getErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error.networkResponse != null)
                    Log.e(MainApplication.TAG, new String(error.networkResponse.data));
                mListener.onError();
            }
        };
    }

    protected Map<String, String> getHeaders() {
        Map<String, String> headers = new HashMap<>();
        if (Client.getInstance().getAccessToken() != null) {
            headers.put("authorization", "Bearer " + Client.getInstance().getAccessToken().value);
        }
        return headers;
    }

    protected Map<String, String> getParams() {
        return new HashMap<>();
    }

    protected abstract com.android.volley.Request getHttpRequest();

    public interface Listener<K> {
        void onResponse(K response);

        void onError();
    }
}
