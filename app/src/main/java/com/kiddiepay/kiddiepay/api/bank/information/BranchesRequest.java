package com.kiddiepay.kiddiepay.api.bank.information;

import com.kiddiepay.kiddiepay.api.JsonArrayRequest;

/**
 * Get a list of bank branches
 */

public class BranchesRequest extends JsonArrayRequest {

    public BranchesRequest(Listener listener) {
        super(listener);
    }

    @Override
    public String getUrl() {
        return "/api/bank-info/branches";
    }

    @Override
    protected int getRequestMethod() {
        return com.android.volley.Request.Method.GET;
    }

}
