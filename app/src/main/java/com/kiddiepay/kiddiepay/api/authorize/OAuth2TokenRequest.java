package com.kiddiepay.kiddiepay.api.authorize;

import com.kiddiepay.kiddiepay.api.Client;
import com.kiddiepay.kiddiepay.api.JsonObjectRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Request Access Tokens
 */

public class OAuth2TokenRequest extends JsonObjectRequest {
    /**
     * Type of Grant, use {@link GrantType}.
     */
    public String grantType = GrantType.CLIENT_CREDENTIALS;
    /**
     * Authorization code provided by the /oauth2/authorize endpoint
     */
    public String code;
    /**
     * Required only if the redirect_uri parameter was included in the authorization request
     * /oauth2/authorize; their values MUST be identical.
     */
    public String redirect_uri = "http://kiddiepay.limepage.studio";
    /**
     * Resource owner username
     */
    public String username;
    /**
     * Resource owner password
     */
    public String password;
    /**
     * Scope being requested
     */
    public String scope = "all";

    public OAuth2TokenRequest(Listener<JSONObject> listener) {
        super(listener);
    }

    @Override
    public String getUrl() {
        return "/oauth2/token";
    }

    @Override
    protected int getRequestMethod() {
        return com.android.volley.Request.Method.POST;
    }

    @Override
    protected Map<String, String> getHeaders() {
        HashMap<String, String> headers = (HashMap<String, String>) super.getHeaders();
        headers.remove("authorization");
        return headers;
    }

    @Override
    protected Map<String, String> getParams() {
        // Cast params from parent.
        HashMap<String, String> params = (HashMap<String, String>) super.getParams();
        // Populate common parameters.
        params.put("grant_type", this.grantType);
        // Populate parameters base on grant type.
        switch (this.grantType) {
            case GrantType.CLIENT_CREDENTIALS:
                params.put("client_id", Client.ID);
                params.put("client_secret", Client.SECRET);
                params.put("scope", this.scope);
                break;
            case GrantType.AUTHORIZATION_CODE:
                params.put("client_id", Client.ID);
                params.put("client_secret", Client.SECRET);
                params.put("code", this.code);
                params.put("redirect_uri", this.redirect_uri);
                break;
        }

        return params;
    }

    public interface GrantType {
        String AUTHORIZATION_CODE = "authorization_code";
        String PASSWORD           = "password";
        String CLIENT_CREDENTIALS = "client_credentials";
        String REFRESH_TOKEN      = "refresh_token";
    }
}
