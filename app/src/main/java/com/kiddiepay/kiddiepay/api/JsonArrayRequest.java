package com.kiddiepay.kiddiepay.api;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;

import org.json.JSONArray;

import java.util.HashMap;
import java.util.Map;

public abstract class JsonArrayRequest extends Request<JSONArray, JSONArray> {
    public JsonArrayRequest(Listener<JSONArray> listener) {
        super(listener);
    }

    @Override
    protected Response.Listener<JSONArray> getListener() {
        return new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                JsonArrayRequest.this.mListener.onResponse(response);
            }
        };
    }

    @Override
    protected Map<String, String> getHeaders() {
        HashMap<String, String> headers = (HashMap<String, String>) super.getHeaders();
        headers.put("accept", "application/json");
        return headers;
    }

    protected com.android.volley.Request getHttpRequest() {
        String url = Client.BASE_URL.concat(getUrl());
        return new com.android.volley.toolbox.JsonArrayRequest(getRequestMethod(), url, null,
                                                               getListener(),
                                                               getErrorListener()) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return JsonArrayRequest.this.getHeaders();
            }
        };
    }
}
