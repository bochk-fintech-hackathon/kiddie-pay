package com.kiddiepay.kiddiepay.api.customer.operations;

import com.android.volley.Request;
import com.kiddiepay.kiddiepay.api.JsonObjectRequest;

import org.json.JSONObject;

/**
 * Created by alexm on 3/10/2018.
 */

public class ProfileRequest extends JsonObjectRequest {
    public ProfileRequest(Listener<JSONObject> listener) {
        super(listener);
    }

    @Override
    public String getUrl() {
        return "/api/customer/profile";
    }

    @Override
    protected int getRequestMethod() {
        return Request.Method.GET;
    }
}
