package com.kiddiepay.kiddiepay.api;

import com.kiddiepay.kiddiepay.api.authorize.AccessToken;
import com.kiddiepay.kiddiepay.api.authorize.OAuth2TokenRequest;
import com.kiddiepay.kiddiepay.api.customer.Profile;

import org.json.JSONObject;

/**
 * Bank of China (Hong Kong) application programming interface client.
 */

public class Client {
    public static final String BASE_URL = "https://api.au.apiconnect.ibmcloud" +
                                          ".com/bochkhackathon-2018/sandbox";
    public static final String ID       = "4688f2a7-d92f-4a4b-8c72-e6e64eee4674";
    public static final String SECRET   = "jW6eB3cF1sW5dN0oM2wM7iG0rU1bT4lJ5nJ3kT0tO8mQ4uD4gE";

    private static Client      sSingleton;
    /**
     * For simplicity and demo purpose, we place customer profile and device token here.
     */
    public         Profile     customerProfile;
    private        AccessToken mAccessToken;

    private Client() {

    }

    public static void initialize() {
        sSingleton = new Client();
    }

    /**
     * Retrieve an instance of {@link Client}.
     *
     * @return Client
     */
    public static Client getInstance() {
        if (sSingleton == null)
            throw new IllegalStateException("Api client has to be initialized before use.");

        return sSingleton;
    }

    /**
     * Authorize API client using client credentials.
     */
    public void authorize(final AuthorizationListener authorizationListener) {
        // Send OAuth2TokenRequest.
        this.send(new OAuth2TokenRequest(new Request.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Client.this.mAccessToken = new AccessToken(response);
                if (!"".equals(Client.this.mAccessToken.value)) {
                    authorizationListener.onSuccess();
                }
            }

            @Override
            public void onError() {
                authorizationListener.onFailed();
            }
        }));
    }

    /**
     * Authorize API client using authorization code.
     */
    public void authorize(String code, final AuthorizationListener authorizationListener) {
        OAuth2TokenRequest request = new OAuth2TokenRequest(new Request.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Client.this.mAccessToken = new AccessToken(response);
                if (!"".equals(Client.this.mAccessToken.value)) {
                    authorizationListener.onSuccess();
                }
            }

            @Override
            public void onError() {
                authorizationListener.onFailed();
            }
        });
        request.grantType = OAuth2TokenRequest.GrantType.AUTHORIZATION_CODE;
        request.code = code;
        // Send OAuth2TokenRequest.
        this.send(request);
    }

    /**
     * Send a request to api.
     *
     * @param request Request
     */
    public void send(Request request) {
        com.kiddiepay.kiddiepay.http.Client httpClient = com.kiddiepay.kiddiepay.http.Client
                .getInstance();
        httpClient.add(request.getHttpRequest());
    }

    public AccessToken getAccessToken() {
        return mAccessToken;
    }

    public interface AuthorizationListener {
        void onSuccess();

        void onFailed();
    }
}
