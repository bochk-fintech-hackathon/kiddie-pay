package com.kiddiepay.kiddiepay.api.customer;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by alexm on 3/10/2018.
 */

public class Profile {
    public String fullName;
    public String address1;
    public String address2;
    public String address3;
    public String phoneNumber;
    public double primaryAmount;
    public double savingAmount;
    public double payingAmount;

    public Profile(JSONObject jsonObject) {
        try {
            this.fullName = jsonObject.getString("full_name");
            this.address1 = jsonObject.getString("address1");
            this.address2 = jsonObject.getString("address2");
            this.address3 = jsonObject.getString("address3");
            this.phoneNumber = jsonObject.getString("phone_no");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        this.primaryAmount = Math.random() * 990000 + 10000;
        this.savingAmount = Math.random() * 900000 + 100000;
        this.payingAmount = Math.random() * 90000 + 10000;
    }
}
