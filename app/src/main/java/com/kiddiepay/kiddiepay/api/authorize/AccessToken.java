package com.kiddiepay.kiddiepay.api.authorize;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Access Token
 */

public class AccessToken {
    public String type;
    public String value;
    public int    expiresIn;
    public String scope;
    public String refreshToken;

    public AccessToken(JSONObject jsonObject) {
        try {
            this.type = jsonObject.getString("token_type");
            this.value = jsonObject.getString("access_token");
            this.expiresIn = jsonObject.getInt("expires_in");
            this.scope = jsonObject.getString("scope");
            if (jsonObject.has("refresh_token"))
                this.refreshToken = jsonObject.getString("refresh_token");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
