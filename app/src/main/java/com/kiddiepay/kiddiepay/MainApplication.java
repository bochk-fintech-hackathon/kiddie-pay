package com.kiddiepay.kiddiepay;

import android.app.Application;

import com.kiddiepay.kiddiepay.database.Client;

/**
 * Created by alexm on 3/9/2018.
 */

public class MainApplication extends Application {
    public static final String TAG = "KiddiePay";

    @Override
    public void onTerminate() {
        super.onTerminate();

        Client.dispose();
    }
}
