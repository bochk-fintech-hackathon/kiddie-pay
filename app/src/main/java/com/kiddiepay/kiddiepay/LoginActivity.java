package com.kiddiepay.kiddiepay;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.kiddiepay.kiddiepay.api.Client;
import com.kiddiepay.kiddiepay.api.Request;
import com.kiddiepay.kiddiepay.api.customer.Profile;
import com.kiddiepay.kiddiepay.api.customer.operations.ProfileRequest;

import org.json.JSONObject;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class LoginActivity extends AppCompatActivity {
    protected WebView mWebView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.initializeLayout();
    }

    private void initializeLayout() {
        setContentView(R.layout.activity_login);

        mWebView = findViewById(R.id.webview);
        mWebView.setWebViewClient(getWebViewClient());
        mWebView.loadUrl(Client.BASE_URL
                                 .concat("/oauth2/authorize?response_type=code&client_id=4688f2a7-d92f" +
                                         "-4a4b-8c72-e6e64eee4674&redirect_uri=http://kiddiepay.limepage" +
                                         ".studio&scope=all"));
    }

    private WebViewClient getWebViewClient() {
        return new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                Log.d(MainApplication.TAG, view.getUrl());
                if (url.contains("http://kiddiepay.limepage.studio/?code=")) {
                    setContentView(R.layout.activity_loading);
                    String code = url.substring(39);
                    Client.getInstance().authorize(code, getAuthorizationListener());
                }
            }
        };
    }

    /**
     * Get customer's profile.
     */
    private void getCustomerProfile() {
        Client.getInstance().send(new ProfileRequest(getProfileListener()));
    }

    /**
     * Get user data from database.
     */
    private void getCustomerUser() {
        new Thread() {
            @Override
            public void run() {
                Statement statement = null;
                ResultSet resultSet = null;
                try {
                    Profile profile = Client.getInstance().customerProfile;
                    statement = com.kiddiepay.kiddiepay.database.Client.getInstance()
                                                                       .createStatement();
                    resultSet = statement.executeQuery(String.format("SELECT * FROM `users` " +
                                                                     "WHERE " +
                                                                     "`full_name`" +
                                                                     " = '%s' AND" +
                                                                     " `phone_no` = '%s'", profile.fullName, profile
                                                                             .phoneNumber));

                    // Both name and phone number are primary keys, so there can only be 1 or 0 result.
                    Runnable resultRunnable;
                    if (resultSet.first()) {
                        // Update device token.
                        updateDeviceToken();

                        switch (resultSet.getString("type")) {
                            case "PARENT":
                                resultRunnable = new Runnable() {
                                    @Override
                                    public void run() {
                                        startActivity(new Intent(LoginActivity.this, ParentManageActivity.class));
                                        finish();
                                    }
                                };
                                break;
                            case "CHILD":
                                resultRunnable = new Runnable() {
                                    @Override
                                    public void run() {
                                        startActivity(new Intent(LoginActivity.this, ChildrenActivity.class));
                                        finish();
                                    }
                                };
                                break;
                            default:
                                throw new IllegalStateException("Unknown type of user account.");
                        }
                    } else {
                        resultRunnable = new Runnable() {
                            @Override
                            public void run() {
                                finish(); // User does not exist in db, we do not handle registration.
                            }
                        };
                    }
                    LoginActivity.this.runOnUiThread(resultRunnable);
                } catch (Exception e) {
                    e.printStackTrace();
                    finish();
                } finally {
                    if (resultSet != null)
                        try {
                            resultSet.close();
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    if (statement != null)
                        try {
                            statement.close();
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                }
            }
        }.start();
    }

    private void updateDeviceToken() {
        Statement statement = null;
        try {
            Profile profile = Client.getInstance().customerProfile;
            statement = com.kiddiepay.kiddiepay.database.Client.getInstance()
                                                               .createStatement();
            statement.executeUpdate(String.format("UPDATE `users` SET `device_token` = '%s' " +
                                                  "WHERE `full_name` = '%s' AND `phone_no` = " +
                                                  "'%s'", com.google.firebase.iid
                                                          .FirebaseInstanceId.getInstance().getToken(), profile
                                                          .fullName, profile
                                                          .phoneNumber));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (statement != null)
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
        }
    }

    private Client.AuthorizationListener getAuthorizationListener() {
        return new Client.AuthorizationListener() {
            @Override
            public void onSuccess() {
                getCustomerProfile();
            }

            @Override
            public void onFailed() {
                LoginActivity.this.initializeLayout();
            }
        };
    }

    private Request.Listener<JSONObject> getProfileListener() {
        return new Request.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Client.getInstance().customerProfile = new Profile(response);
                getCustomerUser();
            }

            @Override
            public void onError() {
                getCustomerProfile();
            }
        };
    }
}
