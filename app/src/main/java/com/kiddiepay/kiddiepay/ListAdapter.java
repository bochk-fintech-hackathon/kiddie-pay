package com.kiddiepay.kiddiepay;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ListAdapter extends BaseAdapter {

    private final Activity context;
    private static LayoutInflater inflater=null;
    private final Integer[] imageId;
    private final String[] colorId;
    private final String[] textId;
    private final String[] title;

    public ListAdapter(Activity a, Integer[] imageId, String[] title,String [] colorId,String [] textId) {
        context = a;
        this.imageId = imageId;
        this.colorId = colorId;
        this.title = title;
        this.textId = textId;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return imageId.length;
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View vi = inflater.inflate(R.layout.list_row, null);
        ImageView imageView = (ImageView) vi.findViewById(R.id.list_image);
        TextView textview = (TextView) vi.findViewById(R.id.title);
        textview.setTextColor(Color.parseColor(textId[i]));
        View bgview = (View) vi.findViewById(R.id.background);
        bgview.setBackgroundColor(Color.parseColor(colorId[i]));
        imageView.setImageResource(imageId[i]);
        textview.setText(title[i]);
        return vi;
    }
}