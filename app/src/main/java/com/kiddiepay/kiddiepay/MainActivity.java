package com.kiddiepay.kiddiepay;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.ashokvarma.bottomnavigation.BottomNavigationBar;
import com.ashokvarma.bottomnavigation.BottomNavigationItem;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.kiddiepay.kiddiepay.api.Client;
import com.kiddiepay.kiddiepay.api.Request;
import com.kiddiepay.kiddiepay.api.bank.information.BranchesRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class MainActivity extends AppCompatActivity implements OnMapReadyCallback,
                                                               GoogleApiClient.ConnectionCallbacks,
                                                               GoogleApiClient.OnConnectionFailedListener,
                                                               com.google.android.gms.location.LocationListener {
    // Request Codes
    private static int REQUEST_LOCATION_PERMISSION = 1000;

    // Properties
    Handler  handler;
    TextView mTemperatureText, mMaxTemperatureText, mMinTemperatureText;
    ImageButton mWebsiteButton, mMenuButton;
    Button              mLoginButton;
    BottomNavigationBar mBottomNavigationBar;
    SlidingMenu         mMenu;
    GoogleApiClient     mGoogleApiClient;
    String[]  title     = {
            "Home",
            "Login"
    };
    String[]  color     = {
            "#C50028",
            "#FFFFFF"
    };
    String[]  textcolor = {
            "#FFFFFF",
            "#000000"
    };
    Integer[] imageId   = {
            R.drawable.home_white,
            R.drawable.login_black
    };
    private GoogleMap mMap;

    // Location Services
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationRequest             mLocationRequest;
    private LocationCallback            mLocationCallback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Bottom Navigation Bar
        mBottomNavigationBar = findViewById(R.id.bottom_navigation_bar);

        mBottomNavigationBar.setActiveColor(R.color.colorRed);

        mBottomNavigationBar
                .addItem(new BottomNavigationItem(R.drawable.home, "Home"))
                .addItem(new BottomNavigationItem(R.drawable.wallet, "e-Wallet"))
                .addItem(new BottomNavigationItem(R.drawable.loan, "Personal Loan"))
                .addItem(new BottomNavigationItem(R.drawable.checkbook, "e-Cheque"))
                .addItem(new BottomNavigationItem(R.drawable.setting, "Setting"))
                .initialise();

        mBottomNavigationBar.setTabSelectedListener(new BottomNavigationBar.OnTabSelectedListener() {
            @Override
            public void onTabSelected(int position) {
            }

            @Override
            public void onTabUnselected(int position) {
            }

            @Override
            public void onTabReselected(int position) {
            }
        });

        //SideMenu
        mMenu = new SlidingMenu(this);
        mMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);

        mMenu.setMode(SlidingMenu.LEFT);
        mMenu.setBehindWidthRes(R.dimen.slidingmenu_offset);
        mMenu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
        //mMenu.setMenu(R.layout.slidingmenu);

        View view = new CustomListView(getApplicationContext(), this, title, color, textcolor, imageId).getActvity();

        mMenu.setMenu(view);
        mMenu.setFadeDegree(0.35f);

        mMenuButton = findViewById(R.id.sidemenu);
        mMenuButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (mMenu.isMenuShowing()) {
                    mMenu.showContent();
                } else {
                    mMenu.showMenu();
                }
            }
        });

        // Login Button
        mLoginButton = findViewById(R.id.login);
        mLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, LoginActivity.class));
            }
        });


        //Temperature UI
        mTemperatureText = findViewById(R.id.tempreature);
        mMaxTemperatureText = findViewById(R.id.max);
        mMinTemperatureText = findViewById(R.id.min);

        //Website
        mWebsiteButton = findViewById(R.id.website);
        mWebsiteButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse("http://www.bochk.com"));
                startActivity(intent);
            }
        });

        //Update Weather Data
        handler = new Handler();
        updateWeatherData();

        // Google Map
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }


    private void updateWeatherData() {
        new Thread() {
            public void run() {
                final JSONObject json = RemoteFetch.getJSON(getApplicationContext());
                Log.d("json", json.toString());
                try {
                    final JSONObject main = json.getJSONObject("main");

                    final Double tempreature       = main.getDouble("temp") - 273.15;
                    final Double lower_tempreature = main.getDouble("temp_min") - 273.15;
                    final Double upper_tempreature = main.getDouble("temp_max") - 273.15;

                    runOnUiThread(new Runnable() {
                        @SuppressLint("DefaultLocale")
                        @Override
                        public void run() {
                            mTemperatureText.setText(String.format("%.0f", tempreature));
                            mMaxTemperatureText.setText(String.format("%.0f", upper_tempreature));
                            mMinTemperatureText.setText(String.format("%.0f", lower_tempreature));
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }.start();
    }


    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    }

    @Override
    public void onLocationChanged(Location location) {
        if (mMap == null)
            return;

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(location.getLatitude(), location.getLongitude()))
                .zoom(15.0f)
                .build();

        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Location Services
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        // Request location permissions.
        this.checkLocationPermissions();

        // Get all branches.
        Client.getInstance().send(new BranchesRequest(this.getBranchesListener()));
    }

    private Request.Listener<JSONArray> getBranchesListener() {
        return new Request.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                mMap.clear();
                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject branch   = response.getJSONObject(i);
                        JSONObject position = branch.getJSONObject("address_coordinates");
                        LatLng location = new LatLng(position.getDouble("latitude"), position
                                .getDouble("longitude"));
                        MarkerOptions marker = new MarkerOptions();
                        marker.position(location);
                        marker.title(branch.getString("name"));
                        mMap.addMarker(marker);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onError() {
                // Retry
                Client.getInstance()
                      .send(new BranchesRequest(MainActivity.this.getBranchesListener()));
            }
        };
    }

    private void checkLocationPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // Check location permissions
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest
                        .permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION_PERMISSION);
                return;
            }
        }

        // Return if google map isn't initialized yet.
        if (mMap == null)
            return;

        // Permission granted.
        mMap.setMyLocationEnabled(true);
        // Get last location
        mFusedLocationClient.getLastLocation()
                            .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                                @Override
                                public void onSuccess(Location location) {
                                    MainActivity.this.onLocationChanged(location);
                                }
                            });

        // Start location update.
        mLocationRequest = LocationRequest.create();
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null)
                    return;

                MainActivity.this.onLocationChanged(locationResult.getLastLocation());
            }
        };

        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                                                    mLocationCallback,
                                                    null /* Looper */);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_LOCATION_PERMISSION) {
            this.checkLocationPermissions();
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onBackPressed() {
        if (mMenu.isMenuShowing()) {
            mMenu.showContent();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.checkLocationPermissions();
    }

    @Override
    protected void onPause() {
        super.onPause();

        // Stop location updates.
        if (mFusedLocationClient != null && mLocationCallback != null) {
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);
            mLocationCallback = null;
        }
    }
}
