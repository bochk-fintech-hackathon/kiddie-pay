package com.kiddiepay.kiddiepay;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import java.util.ArrayList;


public class ControlAdapter extends BaseAdapter {

    private final Activity context;
    private String[] title;
    private String[] description;
    private String[] amount;
    private String[] time;
    private Integer[] ImageId;
    private static LayoutInflater inflater=null;


    public ControlAdapter(Activity context,String[] title,String[] description,Integer[] ImageId,String[] amount,String[] time) {
        this.context = context;
        this.title = title;
        this.description = description;
        this.ImageId = ImageId;
        this.amount = amount;
        this.time = time;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return title.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View vi = inflater.inflate(R.layout.list_track, null);
        TextView header = (TextView) vi.findViewById(R.id.textview);
        header.setText(title[i]);
        TextView descrip = (TextView) vi.findViewById(R.id.example);
        descrip.setText(description[i]);
        TextView tim = (TextView) vi.findViewById(R.id.time);
        tim.setText(time[i]);
        TextView amo = (TextView) vi.findViewById(R.id.amount);
        amo.setText(amount[i]);
        ImageView image = (ImageView) vi.findViewById(R.id.icon);
        image.setImageResource(ImageId[i]);

        return vi;
    }
}
