package com.kiddiepay.kiddiepay;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kiddiepay.kiddiepay.api.Client;
import com.kiddiepay.kiddiepay.api.customer.Profile;


public class MainFragment extends Fragment {

    public MainFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static MainFragment newInstance() {
        MainFragment fragment = new MainFragment();
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        Profile profile = Client.getInstance().customerProfile;

        // Update user's name
        TextView usernameText = view.findViewById(R.id.username_text);
        usernameText.setText(profile.fullName);

        // Update wallet amount.
        TextView primaryAmountText = view.findViewById(R.id.primaryAmountText);
        primaryAmountText.setText(String.format("$%,.2f", profile.primaryAmount));
        TextView savingAmountText = view.findViewById(R.id.savingAmountText);
        savingAmountText.setText(String.format("$%,.2f", profile.savingAmount));
        TextView payingAmountText = view.findViewById(R.id.payingAmountText);
        payingAmountText.setText(String.format("$%,.2f", profile.payingAmount));

        return view;
    }
}
