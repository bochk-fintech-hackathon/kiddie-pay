package com.kiddiepay.kiddiepay;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Switch;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import cn.refactor.library.SmoothCheckBox;


public class ControlFragment extends Fragment {

    ListView listview;
    String[] title = {"Nightlife and drinking","Gambling","Hotels","Airlines","Game"};
    String[] description = {"e.g. Nightclubs and bars","e.g. Casinos","e.g. Hotel reservation","e.g. Flight ticket","e.g. Gaming and virtual coins"};
    Integer[] ImageId ={
            R.drawable.beer,
            R.drawable.gamble,
            R.drawable.hotel,
            R.drawable.airplane,
            R.drawable.game
    };

    public ControlFragment() {
        // Required empty public constructor
    }


    public static ControlFragment newInstance() {
        ControlFragment fragment = new ControlFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        listview = view.findViewById(R.id.listview);
        listview.setAdapter(new SwitchAdapter(getActivity(),title,description,ImageId));


        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
                final CheckListAdapter.Bean bean = (CheckListAdapter.Bean) parent.getAdapter().getItem(position);
                MaterialDialog dialog_confirm = new MaterialDialog.Builder(view.getContext())
                        .title(R.string.ok)
                        .content(R.string.double_confirm)
                        .positiveText(R.string.ok)
                        .negativeText(R.string.cancel)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {
                                bean.isChecked = !bean.isChecked;
                                Switch checkBox = (Switch) view.findViewById(R.id.switch1);
                                checkBox.setChecked(bean.isChecked);
                            }
                        })
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {

                            }
                        })
                        .onAny(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {

                            }
                        })
                        .negativeColorRes(R.color.colorRed)
                        .build();

                dialog_confirm.show();
            }
        });


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_control, container, false);
    }
}
