package com.kiddiepay.kiddiepay;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.ashokvarma.bottomnavigation.BottomNavigationBar;
import com.ashokvarma.bottomnavigation.BottomNavigationItem;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;


public class ParentManageActivity extends AppCompatActivity implements BottomNavigationBar.OnTabSelectedListener {
    SlidingMenu menu;
    ImageButton menubtn, add;
    BottomNavigationBar bottomNavigationBar;
    Fragment selectedFragment = null;

    String[] title     = {
            "Home",
            "Setting",
            "Logout"
    };
    String[] color     = {
            "#C50028",
            "#FFFFFF",
            "#FFFFFF"
    };
    String[] textcolor = {
            "#FFFFFF",
            "#000000",
            "#000000"
    };

    Integer[] imageId = {
            R.drawable.home_white,
            R.drawable.setting,
            R.drawable.login_black
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parent_manage);

        // SideMenu
        menu = new SlidingMenu(this);
        menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);

        menu.setMode(SlidingMenu.LEFT);
        menu.setBehindWidthRes(R.dimen.slidingmenu_offset);
        menu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);

        View view = new CustomListView(getApplicationContext(), this, title, color, textcolor, imageId).getActvity();
        menu.setMenu(view);
        menu.setFadeDegree(0.35f);

        menubtn = findViewById(R.id.sidemenu);
        menubtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (menu.isMenuShowing()) {
                    menu.showContent();
                } else {
                    menu.showMenu();
                }
            }
        });

        selectedFragment = MainFragment.newInstance();
        android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout, selectedFragment);
        transaction.commit();


        // Bottom Navigation Bar
        bottomNavigationBar = findViewById(R.id.bottom_navigation_bar);

        bottomNavigationBar.setActiveColor(R.color.colorRed);

        bottomNavigationBar
                .addItem(new BottomNavigationItem(R.drawable.home, "Home"))
                .addItem(new BottomNavigationItem(R.drawable.cash, "Allowance"))
                .addItem(new BottomNavigationItem(R.drawable.control, "Chore"))
                .addItem(new BottomNavigationItem(R.drawable.chore, "Control"))
                .addItem(new BottomNavigationItem(R.drawable.payment_history, "Payment History"))
                .initialise();

        bottomNavigationBar.setTabSelectedListener(this);

        //Add Button
        add = findViewById(R.id.add_btn);
        add.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onTabSelected(int position) {
        switch (position) {
            case 0:
                add.setVisibility(View.INVISIBLE);
                selectedFragment = MainFragment.newInstance();
                break;
            case 1:
                add.setVisibility(View.INVISIBLE);
                selectedFragment = AllowanceFragment.newInstance();

                break;
            case 2:
                add.setVisibility(View.VISIBLE);
                add.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        MaterialDialog dialog = new MaterialDialog.Builder(view.getContext())
                                .title(R.string.input)
                                .customView(R.layout.popup, true)
                                .positiveText(R.string.ok)
                                .negativeText(R.string.cancel)
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(MaterialDialog dialog, DialogAction which) {

                                    }
                                })
                                .onNegative(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(MaterialDialog dialog, DialogAction which) {

                                    }
                                })
                                .onAny(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(MaterialDialog dialog, DialogAction which) {

                                    }
                                })
                                .negativeColorRes(R.color.colorRed)
                                .build();

                        dialog.show();

                    }
                });

                selectedFragment = ChoreFragment.newInstance();
                break;
            case 3:
                add.setVisibility(View.INVISIBLE);
                selectedFragment = ControlFragment.newInstance();
                break;
            case 4:
                add.setVisibility(View.INVISIBLE);
                selectedFragment = PaymentHistoryFragment.newInstance();
                break;
        }
        android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout, selectedFragment);
        transaction.commit();
    }

    @Override
    public void onTabUnselected(int position) {
        System.out.print(position);
    }

    @Override
    public void onTabReselected(int position) {
        System.out.print(position);
    }
}
