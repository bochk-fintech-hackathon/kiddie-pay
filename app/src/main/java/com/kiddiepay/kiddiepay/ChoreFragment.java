package com.kiddiepay.kiddiepay;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.io.Serializable;
import java.util.ArrayList;

import cn.refactor.library.SmoothCheckBox;


public class ChoreFragment extends Fragment {
    ListView listview;
    String[] title = {"Tutorial Attendance"};
    String[] description = {"100% attendance in tutorial class"};
    String[] reward = {"$100.00"};

    public ChoreFragment() {
    }

    public static ChoreFragment newInstance() {
        ChoreFragment fragment = new ChoreFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        listview = view.findViewById(R.id.listview);
        listview.setAdapter(new CheckListAdapter(getActivity(),title,description,reward));

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> parent, final View view, final int position, long id) {

                final CheckListAdapter.Bean bean = (CheckListAdapter.Bean) parent.getAdapter().getItem(position);
                MaterialDialog dialog = new MaterialDialog.Builder(view.getContext())
                        .title(R.string.ok)
                        .content(R.string.double_confirm)
                        .positiveText(R.string.ok)
                        .negativeText(R.string.cancel)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {
                                bean.isChecked = !bean.isChecked;
                                SmoothCheckBox checkBox = (SmoothCheckBox) view.findViewById(R.id.scb);
                                checkBox.setChecked(bean.isChecked, true);
                            }
                        })
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {

                            }
                        })
                        .onAny(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {

                            }
                        })
                        .negativeColorRes(R.color.colorRed)
                        .build();

                if (!bean.isChecked){
                    dialog.show();
                }

            }
        });


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_chore, container, false);
    }

}
