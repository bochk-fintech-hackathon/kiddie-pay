package com.kiddiepay.kiddiepay;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jaredrummler.materialspinner.MaterialSpinner;

public class AllowanceFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER


    public AllowanceFragment() {
        // Required empty public constructor
    }


    public static AllowanceFragment newInstance() {
        AllowanceFragment fragment = new AllowanceFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        MaterialSpinner amount_dropdown = (MaterialSpinner) view.findViewById(R.id.amount_dropdown);
        MaterialSpinner freq_dropdown = (MaterialSpinner) view.findViewById(R.id.freq_dropdown);
        MaterialSpinner from_dropdown = (MaterialSpinner) view.findViewById(R.id.from_dropdown);
        MaterialSpinner to_dropdown = (MaterialSpinner) view.findViewById(R.id.to_dropdown);

        amount_dropdown.setItems("$25.00", "$50.00", "$100.00", "$250.00", "$500.00");
        amount_dropdown.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

            @Override public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                Log.d("Amount", item);
            }
        });

        freq_dropdown.setItems("Daily", "Weekly", "Monthly");
        freq_dropdown.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

            @Override public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                Log.d("Frequency", item);
            }
        });

        from_dropdown.setItems("Saving Account", "Current Account");
        from_dropdown.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

            @Override public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                Log.d("From", item);
            }
        });

        to_dropdown.setItems("Johnny's Primary","Johnny's Saving", "Johnny's Paying");
        to_dropdown.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

            @Override public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                Log.d("To", item);
            }
        });

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_allowance, container, false);
        return view;
    }
}
