package com.kiddiepay.kiddiepay.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by alexm on 3/10/2018.
 */

public class Client {
    private static Client     sSingleton;
    private        Connection connection;

    private Client() throws SQLException {
        connection = DriverManager.getConnection("jdbc:mysql://database.limepage.studio:3306/" +
                                                 "kiddie_pay",
                                                 "kiddie_pay",
                                                 "P6ZKHNw2eHE8yzGn");
    }

    public static void initialize() throws SQLException {
        sSingleton = new Client();
    }

    public static void dispose() {
        if (sSingleton == null) {
            return;
        }

        try {
            sSingleton.connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        sSingleton.connection = null;
        sSingleton = null;
    }

    public static Client getInstance() {
        if (sSingleton == null)
            throw new IllegalStateException("Database client is not initialized before use.");

        return sSingleton;
    }

    public Statement createStatement() throws SQLException {
        return connection.createStatement();
    }
}
