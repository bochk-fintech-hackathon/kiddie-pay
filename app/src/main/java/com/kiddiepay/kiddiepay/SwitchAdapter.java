package com.kiddiepay.kiddiepay;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import java.util.ArrayList;


public class SwitchAdapter extends BaseAdapter {

    private final Activity context;
    private String[] title;
    private String[] description;
    private Integer[] ImageId;
    private static LayoutInflater inflater=null;
    private ArrayList<CheckListAdapter.Bean> mList = new ArrayList<>();

    public SwitchAdapter(Activity context,String[] title,String[] description,Integer[] ImageId) {
        this.context = context;
        this.title = title;
        this.description = description;
        this.ImageId = ImageId;
        for (int i = 0; i < title.length; i ++) {
            mList.add(new CheckListAdapter.Bean());
        }
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return title.length;
    }

    @Override
    public Object getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View vi = inflater.inflate(R.layout.list_control, null);
        TextView header = (TextView) vi.findViewById(R.id.textview);
        header.setText(title[i]);
        TextView descrip = (TextView) vi.findViewById(R.id.example);
        descrip.setText(description[i]);
        ImageView image = (ImageView) vi.findViewById(R.id.icon);
        image.setImageResource(ImageId[i]);

        Switch onOffSwitch = (Switch) vi.findViewById(R.id.switch1);
        final CheckListAdapter.Bean bean = mList.get(i);
        onOffSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                bean.isChecked = isChecked;
            }

        });

        return vi;
    }
}
