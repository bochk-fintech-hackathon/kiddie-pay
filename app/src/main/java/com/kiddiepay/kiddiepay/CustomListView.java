package com.kiddiepay.kiddiepay;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;

public class CustomListView extends View {

    View v;
    Context context;
    Activity activity;
    ListView list;

    String[] title ;
    String[] color ;
    String[] textcolor;
    Integer[] imageId;

    public CustomListView(Context context,Activity activity, String[] title ,String[] color,String[] textcolor, Integer[] imageId) {
        super(context);
        this.context = context;
        this.activity = activity;
        this.title = title;
        this.color = color;
        this.textcolor = textcolor;
        this.imageId = imageId;
    }

    public View getActvity(){
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = inflater.inflate(R.layout.slidingmenu, null);
        list = v.findViewById(R.id.listview);
        ListAdapter listAdapter = new ListAdapter(activity,imageId,title,color,textcolor);
        list.setAdapter(listAdapter);
        return v;
    }
}
