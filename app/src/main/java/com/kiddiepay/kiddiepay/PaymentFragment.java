package com.kiddiepay.kiddiepay;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.kiddiepay.kiddiepay.http.Client;

import org.json.JSONException;
import org.json.JSONObject;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;


public class PaymentFragment extends Fragment {


    public PaymentFragment() {
        // Required empty public constructor
    }


    public static PaymentFragment newInstance() {
        PaymentFragment fragment = new PaymentFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        MaterialSpinner from_dropdown = view.findViewById(R.id.from_dropdown);
        from_dropdown.setItems("Saving Account", "Current Account");
        from_dropdown.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {

            }
        });

        Button button = view.findViewById(R.id.camera);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //startActivity(new Intent(getActivity(), CameraActivity.class));
            }
        });

        view.findViewById(R.id.confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                notifyParent();
            }
        });
    }

    private void notifyParent() {
        new Thread() {
            @Override
            public void run() {
                Statement statement = null;
                ResultSet resultSet = null;
                try {
                    statement = com.kiddiepay.kiddiepay.database.Client.getInstance()
                                                                       .createStatement();
                    resultSet = statement.executeQuery(String.format("SELECT * FROM `users` " +
                                                                     "WHERE `type` = '%s'",
                                                                     "PARENT"));

                    // Both name and phone number are primary keys, so there can only be 1 or 0 result.
                    if (resultSet.first()) {
                        do {
                            sendPushNotification(resultSet.getString("device_token"));
                        } while (resultSet.next());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (resultSet != null)
                        try {
                            resultSet.close();
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    if (statement != null)
                        try {
                            statement.close();
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                }
            }
        }.start();
    }

    private void sendPushNotification(String deviceToken) throws JSONException {
        JSONObject params = new JSONObject();
        params.put("to", deviceToken);
        JSONObject data = new JSONObject();
        data.put("message", "Johnny is waiting for your permission to make a payment.");
        params.put("data", data);
        JsonObjectRequest request = new JsonObjectRequest("https://fcm.googleapis.com/fcm/send",
                                                          params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(MainApplication.TAG, response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error.networkResponse != null)
                    Log.d(MainApplication.TAG, new String(error.networkResponse.data));
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "key=AIzaSyDQkEPHPomyGdVB3Adtbox2mbEwgiK-Va4");
                return headers;
            }
        };
        Client.getInstance().add(request);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_payment, container, false);
        return view;
    }
}
