package com.kiddiepay.kiddiepay;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

import devlight.io.library.ntb.NavigationTabBar;


public class PaymentHistoryFragment extends Fragment {

    ListView listview;

    String[] title = {"Adidas"};
    String[] description = {"Retail"};
    String[] amount = {"- $100"};
    String[] time = {"Yesterday 4:50pm"};
    Integer[] ImageId ={
            R.drawable.adidas
    };

    public PaymentHistoryFragment() {
        // Required empty public constructor
    }

    public static PaymentHistoryFragment newInstance() {
        PaymentHistoryFragment fragment = new PaymentHistoryFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_payment_history, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        listview = view.findViewById(R.id.listview);
        listview.setAdapter(new ControlAdapter(getActivity(),title,description,ImageId,amount,time));

        final NavigationTabBar navigationTabBar = (NavigationTabBar) view.findViewById(R.id.ntb);
        final ArrayList<NavigationTabBar.Model> models = new ArrayList<>();
        NavigationTabBar.Model model = new NavigationTabBar.Model.Builder(getResources().getDrawable(R.drawable.pending), Color.parseColor("#00000000")
        ).title("Pending").badgeTitle("1").build();
        model.showBadge();

        models.add(model);

        models.add(
                new NavigationTabBar.Model.Builder(getResources().getDrawable(R.drawable.card_setting), Color.parseColor("#00000000")
                ).title("Card Setting").build()
        );

        //TODO redirect page
        navigationTabBar.setModels(models);
        navigationTabBar.setBehaviorEnabled(true);

        navigationTabBar.setOnTabBarSelectedIndexListener(new NavigationTabBar.OnTabBarSelectedIndexListener() {
            @Override
            public void onStartTabSelected(final NavigationTabBar.Model model, final int index) {

            }

            @Override
            public void onEndTabSelected(final NavigationTabBar.Model model, final int index) {
                model.hideBadge();
            }
        });
    }
}
