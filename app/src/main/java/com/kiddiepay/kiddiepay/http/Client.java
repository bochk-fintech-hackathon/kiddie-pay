package com.kiddiepay.kiddiepay.http;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Client for handling HTTP requests and responses.
 */

public class Client {
    private static Client       sSingleton;
    private        RequestQueue mRequestQueue;

    private Client(Context applicationContext) {
        this.mRequestQueue = Volley.newRequestQueue(applicationContext);
    }

    /**
     * Initialize HTTP client.
     *
     * @param applicationContext Application Context
     */
    public static void initialize(Context applicationContext) {
        sSingleton = new Client(applicationContext);
    }

    /**
     * Retrieve an instance of HTTP client. {@link #initialize(Context)} must be called before this.
     *
     * @return HTTP Client
     */
    public static Client getInstance() {
        if (sSingleton == null)
            throw new IllegalStateException("HTTP Client is not initialized before use.");

        return sSingleton;
    }

    /**
     * Adds a Request to the dispatch queue.
     *
     * @param request The request to service
     * @return The passed-in request
     */
    public Request add(Request request) {
        return this.mRequestQueue.add(request);
    }
}
