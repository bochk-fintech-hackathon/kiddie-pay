package com.kiddiepay.kiddiepay;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

public class RemoteFetch {

    public static JSONObject getJSON(Context context){
        try {
            URL url = new URL("http://api.openweathermap.org/data/2.5/weather?id=1819730&APPID=272db853246a134ef571a6447e4f59ba");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            BufferedReader reader =
                    new BufferedReader(new InputStreamReader(connection.getInputStream()));

            StringBuffer json = new StringBuffer(1024);
            String tmp = "";

            while((tmp = reader.readLine()) != null)
                json.append(tmp).append("\n");
            reader.close();

            JSONObject data = null;
            data = new JSONObject(json.toString());

            if(data.getInt("cod") != 200) {
                return null;
            }
            return data;
        }catch(Exception e){
            return null;
        }
    }
}