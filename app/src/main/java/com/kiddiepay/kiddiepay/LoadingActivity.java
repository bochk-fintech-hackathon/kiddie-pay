package com.kiddiepay.kiddiepay;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ProgressBar;

import com.github.ybq.android.spinkit.style.Circle;
import com.kiddiepay.kiddiepay.api.Client;

import java.sql.SQLException;

public class LoadingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);

        ProgressBar progressBar  = findViewById(R.id.spin_kit);
        Circle      doubleBounce = new Circle();
        progressBar.setIndeterminateDrawable(doubleBounce);

        // Initialize HTTP Client.
        com.kiddiepay.kiddiepay.http.Client.initialize(this.getApplicationContext());

        // Initialize API Client.
        Client.initialize();

        // Authorize API Client.
        Client.getInstance().authorize(this.getAuthorizationListener());
    }

    private void showAlertDialog(final DialogInterface.OnClickListener listener) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(LoadingActivity.this);
                builder.setMessage(R.string.no_network_dialog_message)
                       .setTitle(R.string.no_network_dialog_title);
                builder.setNeutralButton(R.string.retry, listener);
                builder.create().show();
            }
        });
    }

    private void connectDatabase() {
        // Initialize Database Client.
        new Thread() {
            @Override
            public void run() {
                try {
                    com.kiddiepay.kiddiepay.database.Client.initialize();
                    startActivity(new Intent(LoadingActivity.this, MainActivity.class));
                    finish();
                } catch (SQLException e) {
                    Log.e(MainApplication.TAG, "SQLException: " + e.getMessage());
                    Log.e(MainApplication.TAG, "SQLState: " + e.getSQLState());
                    Log.e(MainApplication.TAG, "VendorError: " + e.getErrorCode());
                    e.printStackTrace();
                    showAlertDialog(new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            // Try again
                            connectDatabase();
                        }
                    });
                }
            }
        }.start();
    }

    private com.kiddiepay.kiddiepay.api.Client.AuthorizationListener getAuthorizationListener() {
        return new com.kiddiepay.kiddiepay.api.Client.AuthorizationListener() {
            @Override
            public void onSuccess() {
                connectDatabase();
            }

            @Override
            public void onFailed() {
                showAlertDialog(new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // Try again
                        Client.getInstance()
                              .authorize(LoadingActivity.this.getAuthorizationListener());
                    }
                });
            }
        };
    }
}
