package com.kiddiepay.kiddiepay;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;

/**
 * Created by alexm on 3/11/2018.
 */

public class FirebaseInstanceIdService extends com.google.firebase.iid.FirebaseInstanceIdService {
    // Store token here for simplicity and demo purpose.
    public static String DEVICE_TOKEN;

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(MainApplication.TAG, "Refreshed token: " + refreshedToken);

        DEVICE_TOKEN = refreshedToken;
    }

}
