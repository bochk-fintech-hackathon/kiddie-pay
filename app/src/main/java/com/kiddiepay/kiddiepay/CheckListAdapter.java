package com.kiddiepay.kiddiepay;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;

import cn.refactor.library.SmoothCheckBox;

public class CheckListAdapter extends BaseAdapter {

    private final Activity context;
    private static LayoutInflater inflater=null;
    private String[] title;
    private String[] description;
    private String[] reward;

    private ArrayList<Bean> mList = new ArrayList<>();

    public CheckListAdapter(Activity context,String[] title,String[] description,String[] reward) {
        this.context = context;
        this.title = title;
        this.description = description;
        this.reward = reward;

        for (int i = 0; i < title.length; i ++) {
            mList.add(new Bean());
        }

        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return title.length;
    }

    @Override
    public Object getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View vi = inflater.inflate(R.layout.list_checklist, null);
        TextView header = (TextView) vi.findViewById(R.id.textview);
        header.setText(title[i]);
        TextView descrip = (TextView) vi.findViewById(R.id.description);
        descrip.setText(description[i]);
        TextView re = (TextView) vi.findViewById(R.id.reward);
        re.setText(reward[i]);

        SmoothCheckBox checkbox = (SmoothCheckBox) vi.findViewById(R.id.scb);

        final Bean bean = mList.get(i);
        checkbox.setOnCheckedChangeListener(new SmoothCheckBox.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SmoothCheckBox checkBox, boolean isChecked) {
                bean.isChecked = isChecked;
            }
        });

        return vi;
    }

    static class Bean implements Serializable {
        boolean isChecked;
    }

}
